# Introduction

Let’s image that a new application is being developed and you are part of the SRE  
team where your task to take care of the continuous integration and continuous  
deployment pipeline as well as make the application ready for responding to a high  
demand.  Our favorite technologies are: Java, Spring, Docker, Jenkins, AWS,  
Terraform and Kubernetes, but we are open to hear about any other.  

The application source: <https://github.com/guipal/apiSampleJava>  


# Description

Implementation of a pipeline where:  

-   Every commit of each developer goes through phases (steps)
-   Finishing on a production environment.
-   We do not want to limit you on anything. If you do not feel comfortable with  
    implementing something, please just provide some notes on how you would implement  
    it.
-   Please try to stick to the technologies mentioned above. If you have limitations  
    on the implementation on the infrastructure part, please add the scripts or design  
    in order to set it up.

Description of the best practices you would implement to improve application  
observability.  

-   What would you do to enable quick and easy debug of the application?
-   What would you propose to measure and ensure SLA, SLOs are accomplished?

Description of a solution for deploying the application in a high availability  
environment without downtime.  

-   How will you guarantee the resilience of the application?
-   How would you propose the deployment of hotfixes or new features?

Description of the trade off and risks in your design.  

-   Have you considered any other alternatives to your design?

Please include a solution design of the environment on any readable format (a Draw,  
a diagram, etc.), including explanations for every decision made (design, technology  
and observability).  


## Hints

-   Build once run anywhere
-   How easy would it be to check what is going on in your environment?
-   We want to see what your workflow would be for a new feature development in your  
    environment from committing into the svc till running in production.
-   Everything as code is our MOTO.
-   We love automatization.
-   Security is always in our mind.
-   As SRE, there are lots of important concepts you can cover and include in your  
    design.
-   Publish your solution in a public repository (ex. github/gitlab) and share the  
    link with us.


# Quick-start


## Prerequisites

[GIT 2.25.1](https://git-scm.com/downloads), [JDK 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html), [MAVEN 3.8.1](https://ftp.cixug.es/apache/maven/maven-3/3.8.1/binaries/apache-maven-3.8.1-bin.zip), [Docker 19.03.8](https://docs.docker.com/engine/install/), [Docker compose 1.28.5](https://github.com/docker/compose/releases), [VAGRANT 2.2.16](https://www.vagrantup.com/intro)  


## Installation

-   Git clone this repository  
    
    ```shell
    git clone https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git
    cd lll-apisamplejava-pipeline
    ```
-   Run Jenkins environment  
    -   If you already have your own Jenkins environment currently running, just skip this  
        step and proceed to create the Pipeline.
    -   Use Vagrant to get a Jenkins environment with support for **Java**, **Maven** and **Docker** in  
        the master node:  
        
        ```shell
        vagrant up
        # password: vagrant
        ssh -L 8282:localhost:8282 -L 8383:localhost:8080 vagrant@192.168.35.10
        docker login
        sudo su -
        cat /var/lib/jenkins/secrets/initialAdminPassword
        ```
        
        Visit <http://localhost:8282/> to create the Pipeline.
-   Create the Pipeline  
    -   Use the classic UI of Jenkins to create a new Pipeline:  
        ![img](doc/Pipeline01.png)
    -   Use **Pipeline script from SCM** as definition and provide the Git Url:  
        <https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git>  
        ![img](doc/Pipeline02.png)
-   Run the Pipeline  
    In the Jenkins UI Pipeline created, click **Build now**  
    ![img](doc/Pipeline03.png)  
    
        Started by user admin
        Obtained Jenkinsfile from git https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git
        Running in Durability level: MAX_SURVIVABILITY
        [Pipeline] Start of Pipeline
        [Pipeline] node
        Running on Jenkins in /var/jenkins_home/workspace/lll-apisamplejava-pipeline
        [Pipeline] {
        [Pipeline] stage
        [Pipeline] { (Declarative: Checkout SCM)
        [Pipeline] checkout
        Selected Git installation does not exist. Using Default
        The recommended git tool is: NONE
        No credentials specified
        Cloning the remote Git repository
        Cloning repository https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git
         > git init /var/jenkins_home/workspace/lll-apisamplejava-pipeline # timeout=10
        Fetching upstream changes from https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git
         > git --version # timeout=10
         > git --version # 'git version 2.20.1'
         > git fetch --tags --force --progress -- https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git +refs/heads/*:refs/remotes/origin/* # timeout=10
         > git config remote.origin.url https://gitlab.com/lll-tools/spring/lll-apisamplejava-pipeline.git # timeout=10
         > git config --add remote.origin.fetch +refs/heads/*:refs/remotes/origin/* # timeout=10
        Avoid second fetch
         > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
        Checking out Revision 6cf0d059a4447cc290a8d90b620f107eba8ddbbc (refs/remotes/origin/master)
         > git config core.sparsecheckout # timeout=10
         > git checkout -f 6cf0d059a4447cc290a8d90b620f107eba8ddbbc # timeout=10
        Commit message: "Basic pipeline skeleton"
        First time build. Skipping changelog.


# Implementation


## Basic Jenkins Pipeline Stages

We will define the Pipeline in a Jenkinsfile included in the Git project repository,  
alternative the Bluocean Plugin may be used to create the Pipeline.  The Jenkins master  
node must include support for: **Java**, **Maven**, **Docker** and **Kubectl**, alternative a worker  
node with supoort for these tools may me added.  


### Build

The first stage in the Pipeline will be to build the microservice. For the example  
Maven will be used.  

```shell
mvn clean package
```


### Test

The next stage is to test the microservice.  

```shell
mvn test
```


### Docker Build

A new image will be generated with the new release.  

```shell
docker build -t luislain/apisamplejava:1.0 .
```


### Image Scan

A good approach is to scan the image generated for security vulnerabilities.  

```shell
docker scan luislain/apisamplejava:1.0
```


### Docker Registry

The image with the new release will be pushed to the registry.  

```js
script {
  docker.withRegistry( '', registryCredential ) {
    dockerImage.push("$BUILD_NUMBER")
    }
  }
```


### Deploy to Dev

The new release will be deployed in the Development environment.  

```shell
#scp target/apiSample-1.0-SNAPSHOT.jar devuser@devhost:/devpath
kubectl config use-context dev-env
kubectl apply -f apisamplejava.yaml
```


### Deploy to Stage

The new release will be deployed in the Staging environment.  

```shell
kubectl config use-context stage-env
kubectl apply -f apisamplejava.yaml
```


### Deploy to Production

Finally the new release will be deployed in the Production environment.  

```shell
kubectl config use-context prod-env
kubectl apply -f apisamplejava.yaml
```


# Pipeline Improvements


## JFrog Pipeline

<https://www.jfrog.com/confluence/display/JFROG/Pipelines+Step-By-Step>  
Using JFrog Artifactory to define a Image registry and automate the Pipeline stages.  
![img](doc/Pipeline06.png)  
![img](doc/Pipeline07.png)  


## Jenkins-X Pipeline

<https://jenkins-x.io/docs/>  
Having access to a Kubernetes cluster and using Terraform, Jenkis-X may be deployed in  
the cluster to fully manage the Pipeline:  

```shell
terraform output jx_requirements > jx-requirements.yml
jx boot --requirements jx-requirements.yml
```


## Gitlab CD/CI (Autodevops) Pipeline

![img](doc/Pipeline05.png)  


# Observability and SLA

Using Prometheus metrics we could measure the average stage time that takes to execute  
the Pipeline and identify unexpected delays.  


## Prometheus (metrics)

-   Install Node Explorer in the nodes:  
    <https://prometheus.io/docs/guides/node-exporter/>
-   Install Prometheus Jenkis plugin in master node:  
    <https://plugins.jenkins.io/prometheus/>  
    Visit <http://localhost:8282/prometheus>
-   Configure Prometheus server to scrap metrics exposed by Jenkis:  
    
    ```shell
    docker run -d --name prometheus -p 9090:9090 prom/prometheus
    # - job_name: 'jenkins'
    #     metrics_path: /prometheus
    #     static_configs:
    # - targets: ['localhost:8282']
    ```
-   Configure grafana to visualize the metrics that define our SLAs  
    
    ```shell
    docker run -d --name grafana -p 3000:3000 grafana/grafana
    ```
    
    Metrics example:  
    
        default_jenkins_builds_last_stage_duration_milliseconds_summary_count
        default_jenkins_builds_last_stage_duration_milliseconds_summary_sum


## Prometheus (logs)

Use Grok Exporter to analyze Jenkis and nodes logs  
<https://github.com/fstab/grok_exporter>  


# Security


## Dockerfile user

Running applications with user privileges helps to mitigate some risks, modify  
Dockerfile to run the application as a non-root user:  

    FROM openjdk:8-jre-alpine
    RUN addgroup -S spring && adduser -S spring -G spring
    USER spring:spring


## Credentials

Define credentials in Jenkins to access the Git repo and Docker Hub:  
![img](doc/Pipeline04.png)  


## Vulnerabilities


### Dockerhub (Synk)

<https://docs.docker.com/docker-hub/vulnerability-scanning/>  

```shell
docker scan --file Dockerfile luislain/apisamplejava
```


### JFrog (X-Ray)

<https://www.jfrog.com/confluence/display/JFROG/QuickStart+Guide%3A+JFrog+Cloud>  
Define Watches and Policies using X-Ray in JFrog to scan for vulnerabilities in the  
image built before deploying.  


## Post-commit trigger

Implement a post-commit trigger to run the Pipeline from the SCM with new commits  
<https://wiki.jenkins.io/display/JENKINS/Building+a+software+project>  


## Performance

<https://wiki.apache.org/tomcat/HowTo/FasterStartUp#Entropy_Source>  

    2021-06-03 11:25:00.620 INFO 3686 --- [ost-startStop-1]
    o.a.catalina.core.AprLifecycleListener : The APR based Apache Tomcat Native library
    which allows optimal performance in production environments was not found on the
    java.library.path: [/usr/java/packages/lib:/usr/lib/x86_64-linux-gnu/jni:/lib/x86_64-linux-gnu:/usr/lib/x86_64-linux-gnu:/usr/lib/jni:/lib:/usr/lib]


# High Availability


## Resilience of the application


### Create microservice

Having access to a Kubernetes cluster we can deploy the microservice with several  
Replicas and to update the POD to the new release to guarantee the resilience of the applicaiton.  

-   Define deployment yaml file  
    
        apiVersion: apps/v1
        kind: Deployment
        metadata:
          name: apisamplejava
        spec:
          selector:
            matchLabels:
              app: apisamplejava
          replicas: 3
          template:
            metadata:
              labels:
        	app: apisamplejava
            spec:
              containers:
              - name: apisamplejava
        	image: luislain/apisamplejava
        	imagePullPolicy: Always
        	ports:
        	- name: apisamplejava
        	  containerPort: 8080
        	  protocol: TCP
-   Define service yaml file  
    
        apiVersion: v1
        kind: Service
        metadata:
          name: apisamplejava-svc
          labels:
            app: apisamplejava
        spec:
          type: LoadBalancer
          ports:
          - port: 8080
          selector:
            app: apisamplejava


### Deploy microservice in cluster

```shell
kubectl apply -f apisamplejava.yaml
kubectl scale --replicas=3 -f apisamplejava.yaml
```

1.  Terraform

    Create the **main.tf** definition of the cluster  
    
    ```shell
    terraform init
    terraform apply
    ```

2.  EKS

    Create the cluster infrastrucutre  
    
    ```shell
    aws cloudformation create-stack \
    	  --stack-name apisamplejava-vpc \
    	  --template-url https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/amazon-eks-vpc-private-subnets.yaml
    ```
    
    Create the cluster  
    
    ```shell
    aws eks create-cluster --name apisamplejava-cluster \
        --role-arn arn:aws:iam::00000000000:role/apisamplejavaEKSClusterRole \
        --resources-vpc-config \
        subnetIds=subnet-00000000,subnet-0000000001,securityGroupIds=sg-0f000000000
    ```


## Deployment of hotfixes or new features


### Upgrade POD

```shell
kubectl --record deployment.apps/apisamplejava-deployment \
	set image deployment.v1.apps/apisamplejava-deployment \
	apisamplejava=apisamplejava:2.0
```


### Create a Helm Chart

```shell
helm package charts/apisamplejava
```

    Successfully packaged chart and saved it to: /usr/local/src/luislain/lll-tools/spring/apiSampleJava/apisamplejava-0.1.0-SNAPSHOT.tgz


### Install chart in cluster

```shell
helm install apisamplejava-0.1.0-SNAPSHOT.tgz
```


# References

<https://gitlab.com/lll-tools/spring/lll-subscription-system>  
<https://hub.docker.com/repository/docker/luislain/apisamplejava>  
<https://spring.io/guides/gs/spring-boot-docker/>  
<https://spring.io/guides/gs/spring-boot-kubernetes/>  
<https://spring.io/guides/topicals/spring-boot-docker>  
<https://www.metricfire.com/blog/monitoring-cicd-pipelines-metricfire/>  
<https://medium.com/@eng.mohamed.m.saeed/monitoring-jenkins-with-grafana-and-prometheus-a7e037cbb376>  
<https://www.axia-consulting.co.uk/sla-metrics.htm>  
<https://www.businesstechweekly.com/operational-efficiency/outsourcing-and-supplier-management/measure-sla-metrics/>  
<https://www.cio.com/article/2438284/outsourcing-sla-definitions-and-solutions.html>  
<https://blog.paessler.com/4-metrics-for-measuring-your-service-level-agreements>  
<https://www.atlassian.com/itsm/service-request-management/slas>  
<https://spring.io/blog/2018/03/16/micrometer-spring-boot-2-s-new-application-metrics-collector>  
<https://sla4oai.specs.governify.io/Specification.html>  
<https://faun.pub/docker-build-push-with-declarative-pipeline-in-jenkins-2f12c2e43807>  
<https://medium.com/swlh/jenkins-pipeline-to-create-docker-image-and-push-to-docker-hub-721919512f2>  
<https://docs.cloudbees.com/docs/admin-resources/latest/plugins/docker-workflow>  
<https://dzone.com/articles/building-docker-images-to-docker-hub-using-jenkins>  
<https://github.com/micrometer-metrics/micrometer>  
<https://micrometer.io/>  
<https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#deployment.cloud.kubernetes>  
<https://www.jfrog.com/confluence/display/JFROG/Pipelines+Step-By-Step>  
<https://docs.docker.com/engine/scan/#prerequisites>  
<https://helm.sh/docs/helm/helm_install/>  


## Loki (logs)

<https://github.com/grafana/loki>  
<https://grafana.com/docs/loki/latest/clients/promtail/installation/>  
<https://grafana.com/docs/loki/latest/getting-started/get-logs-into-loki/>  
<https://grafana.com/go/observabilitycon/observability-with-logs-grafana/?pg=docs-loki&plcmt=footer-resources-3>  


## Oracle Spring Kubernetes

<https://docs.oracle.com/en-us/iaas/developer-tutorials/tutorials/spring-on-k8s/01oci-spring-k8s-summary.htm>  

```shell
# Create Dockerfile
cat <<EOF > Dockerfile
FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
EOF
# Build and package the app:
mvn package
# Run and test the app:
java -jar target/gs-spring-boot-docker-0.1.0.jar
# Test the app either by entering in a new terminal
curl -X GET http://localhost:8080
#or loading localhost:8080 in a browser (if your environment is local).
#You may need to precede the docker commands with sudo in the following steps.
# Build the Docker images:
docker build -t spring-boot-hello .
# Run your Docker image:
docker run -p 8080:8080 -t spring-boot-hello
# Commit in Registry (OCIR)
docker login --username luislain
#docker tag local-image:tagname new-repo:tagname
#docker push new-repo:tagname
docker push luislain/lll-tools:tagname
# Deploy in Cluster
# First, create a registry secret for your application. This will be used to authenticate your image when it is deployed to your cluster.
kubectl create secret docker-registry ocirsecret \
	--docker-server=<region-key>.ocir.io \
	--docker-username='<tenancy-name>/<user-name>' \
	--docker-password='<auth-token>' \
	--docker-email='<email-address>'
kubectl get secret ocirsecret --output=yaml
#Determine the host URL to your registry image using the following template.
#<region-code>.ocir.io/<tenancy-name>/<repo-name>/<image-name>:<tag>
cat <<EOF > sb-app-yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sbapp
spec:
  selector:
    matchLabels:
      app: sbapp
  replicas: 3
  template:
    metadata:
      labels:
	app: sbapp
    spec:
      containers:
      - name: sbapp
	image: <your-image-url>
	imagePullPolicy: Always
	ports:
	- name: sbapp
	  containerPort: 8080
	  protocol: TCP
      imagePullSecrets:
	- name: <your-secret-name>
---
apiVersion: v1
kind: Service
metadata:
  name: sbapp-lb
  labels:
    app: sbapp
spec:
  type: LoadBalancer
  ports:
  - port: 8080
  selector:
    app: sbapp
EOF
# Deploy app in cluster
kubectl create -f sb-app.yaml
kubectl get service
```
